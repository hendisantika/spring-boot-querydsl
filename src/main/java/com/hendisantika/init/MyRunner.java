package com.hendisantika.init;

import com.hendisantika.model.QCity;
import com.hendisantika.repository.CityRepository;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-querydsl
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/3/22
 * Time: 08:28
 * To change this template use File | Settings | File Templates.
 */
@Component
@SuppressWarnings({"rawtypes", "unchecked"})
@RequiredArgsConstructor
public class MyRunner implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(MyRunner.class);
    private final CityRepository cityRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void run(String... args) throws Exception {

        var qCity = QCity.city;

        var query = new JPAQuery(entityManager);

        query.from(qCity).where(qCity.name.eq("Bratislava")).distinct();
        var c1 = query.fetch();

        logger.info("c1 {}", c1);

        var query2 = new JPAQuery(entityManager);
        query2.from(qCity).where(qCity.name.endsWith("est").and(qCity.population.lt(1800000)));
        var cities = query2.fetch();

        logger.info("cities {}", cities);

        BooleanExpression booleanExpression = qCity.population.goe(2_000_000);
        OrderSpecifier<String> orderSpecifier = qCity.name.asc();
        var cities2 = cityRepository.findAll(booleanExpression, orderSpecifier);

        logger.info("cities2 {}", cities2);
    }
}
