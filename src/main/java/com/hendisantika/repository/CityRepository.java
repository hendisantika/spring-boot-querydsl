package com.hendisantika.repository;

import com.hendisantika.model.City;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-querydsl
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/3/22
 * Time: 08:26
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface CityRepository extends CrudRepository<City, Long>, QuerydslPredicateExecutor<City> {
}
