# Spring Boot Querydsl

### Querydsl

Querydsl is a framework that enables the construction of statically typed SQL-like queries through its fluent API.
Spring Data modules offer integration with Querydsl through QuerydslPredicateExecutor.

### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-querydsl.git`
2. Navigate to the folder: `cd spring-boot-querydsl`
3. Run the application: `mvn clean spring-boot:run`

### Console log:

```shell
03-10-2022 08:36:55 [restartedMain] INFO  com.hendisantika.SpringBootQuerydslApplication.logStartupProfileInfo - No active profile set, falling back to 1 default profile: "default"
03-10-2022 08:36:55 [restartedMain] INFO  org.springframework.boot.devtools.env.DevToolsPropertyDefaultsPostProcessor.logTo - Devtools property defaults active! Set 'spring.devtools.add-properties' to 'false' to disable
03-10-2022 08:36:56 [restartedMain] INFO  org.springframework.data.repository.config.RepositoryConfigurationDelegate.registerRepositoriesIn - Bootstrapping Spring Data JPA repositories in DEFAULT mode.
03-10-2022 08:36:56 [restartedMain] INFO  org.springframework.data.repository.config.RepositoryConfigurationDelegate.registerRepositoriesIn - Finished Spring Data repository scanning in 17 ms. Found 1 JPA repository interfaces.
03-10-2022 08:36:56 [restartedMain] INFO  com.zaxxer.hikari.HikariDataSource.getConnection - HikariPool-1 - Starting...
03-10-2022 08:36:56 [restartedMain] INFO  com.zaxxer.hikari.HikariDataSource.getConnection - HikariPool-1 - Start completed.
03-10-2022 08:36:56 [restartedMain] INFO  org.hibernate.jpa.internal.util.LogHelper.logPersistenceUnitInformation - HHH000204: Processing PersistenceUnitInfo [name: default]
03-10-2022 08:36:56 [restartedMain] INFO  org.hibernate.Version.logVersion - HHH000412: Hibernate ORM core version 5.6.11.Final
03-10-2022 08:36:56 [restartedMain] INFO  org.hibernate.annotations.common.Version.<clinit> - HCANN000001: Hibernate Commons Annotations {5.1.2.Final}
03-10-2022 08:36:56 [restartedMain] INFO  org.hibernate.dialect.Dialect.<init> - HHH000400: Using dialect: org.hibernate.dialect.H2Dialect
03-10-2022 08:36:56 [restartedMain] INFO  org.hibernate.engine.transaction.jta.platform.internal.JtaPlatformInitiator.initiateService - HHH000490: Using JtaPlatform implementation: [org.hibernate.engine.transaction.jta.platform.internal.NoJtaPlatform]
03-10-2022 08:36:56 [restartedMain] INFO  org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean.buildNativeEntityManagerFactory - Initialized JPA EntityManagerFactory for persistence unit 'default'
03-10-2022 08:36:56 [restartedMain] INFO  org.springframework.boot.devtools.autoconfigure.OptionalLiveReloadServer.startServer - LiveReload server is running on port 35729
03-10-2022 08:36:56 [restartedMain] INFO  com.hendisantika.SpringBootQuerydslApplication.logStarted - Started SpringBootQuerydslApplication in 1.169 seconds (JVM running for 1.454)
03-10-2022 08:36:57 [restartedMain] INFO  com.hendisantika.init.MyRunner.run - c1 [City(id=1, name=Bratislava, population=432000)]
03-10-2022 08:36:57 [restartedMain] INFO  com.hendisantika.init.MyRunner.run - cities [City(id=2, name=Budapest, population=1759000), City(id=11, name=Brest, population=139163)]
03-10-2022 08:36:57 [restartedMain] INFO  com.hendisantika.init.MyRunner.run - cities2 [City(id=14, name=Bandung, population=2000000), City(id=10, name=Berlin, population=3671000), City(id=5, name=Los Angeles, population=3971000), City(id=6, name=New York, population=8550000), City(id=8, name=Suzhou, population=4327066), City(id=9, name=Zhengzhou, population=4122087)]
```